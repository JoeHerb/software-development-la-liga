﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using LaLiga;

namespace LaLigaConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            bool matched = false;

            Console.SetWindowSize(120, 50);
            League L1 = new League("La Liga", "Spain", "Athletico Madrid", 1);
            L1.addTeam("Barcelona", 2);
            L1.addTeam("Real Madrid", 3);
            L1.addTeam("Valencia", 4);

            /*=========================== 1. Display all objects and associated classes ==========================*/
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1. Display all Leagues, Teams and Staff Members.\n");
            Console.ForegroundColor = ConsoleColor.White;

            L1.displayAll();
            /*======================== End of 1. Display all objects and associated classes =======================*/


            /*======================================= 2. Add a new Group ==========================================*/
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\n2. Add a new team to the league\n");
            Console.ForegroundColor = ConsoleColor.White;
            L1.addTeam("Villareal", 5);

            foreach (Team teams in L1.teams)
                Console.WriteLine(teams.ToString());
            
            /*==================================== End of 2. Add a new Group ======================================*/


            /*======================= 3. Add an associated Parent/Child object to one Group =======================*/
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\n3. Add a staff member to a team\n");
            Console.ForegroundColor = ConsoleColor.White;
            L1.teams[0].addPlayer("Antoine Greizmann", 15, 1, 4, "ST");
            L1.teams[0].addCoach("Diego Simeone", 5, 2, "Manager");
            L1.teams[0].addTreatment("Dr. Harris", (float)0.5, 3, "Back", "B101");
            L1.teams[1].addPlayer("Lionel Messi", 20, 1, 5, "RW");
            L1.teams[2].addPlayer("Cristiano Ronaldo", 20, 1, 2, "ST");
            L1.teams[3].addPlayer("Alvaro Negredo", 9, 1, 4, "ST");
            L1.teams[4].addPlayer("Roberto Soldado", 1, 1, 2, "ST");

            //Prints the data for the teams
            foreach (Team teams in L1.teams)
            {                
                Console.WriteLine(teams.ToString());

                teams.displayStaff();
            }
            /*==================== End of 3. Add an associated Parent/Child object to one Group ===================*/


            /*============== 4+5. Move an associated child object from one group to another and remove a child object from a group =============*/
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\n4+5. Move a staff member to another team and remove them from their original team.\n");
            Console.ForegroundColor = ConsoleColor.White;

            L1.movePlayer(0, 1, 0);
            /*=========== End of 4+5. Move an associated child object from one group to another and remove a child object from a group ==========*/


            /*======================================================= 6. Remove a Group =========================================================*/
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\n6. Remove a team from the league.");
            Console.ForegroundColor = ConsoleColor.White;

            L1.removeTeam(3);
            /*=================================================== End of 6. Remove a Group ======================================================*/


            /*======================================= 7. List associated Parent/Child objects for one Group =====================================*/
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\n7. List associated staff for a team.\n");
            Console.ForegroundColor = ConsoleColor.White;

            L1.teams[0].displayAll();
            /*======================================= End of 7. List associated Parent/Child objects for one Group =====================================*/


            /*==========================================8. Find GROUP an associated PARENT/CHILD object belongs to=======================================*/
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\n8. Which group/team does Lionel Messi belong to?");
            Console.ForegroundColor = ConsoleColor.White;

            foreach (Team teams in L1.teams)
            {
                teams.getTeam("Lionel Messi");
            }
            /*======================================End of 8. Find GROUP an associated PARENT/CHILD object belongs to====================================*/

            /*==========================================9. List GROUP(s) with no associated PARENT/CHILD object(s)=======================================*/
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\n9. List teams with no associated staff.\n");
            Console.ForegroundColor = ConsoleColor.White;

            foreach (Team teams in L1.teams)
            {
                matched = teams.displayEmtpyTeams(); 
            }

            if (matched == false)
                Console.WriteLine("- All teams have staff members.\n");

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\nLet's add an empty Sevilla team just for testing purposes...\n");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nList teams with no associated staff.");
            Console.ForegroundColor = ConsoleColor.White;
            L1.addTeam("Sevilla", 5);

            foreach (Team teams in L1.teams)
            {
                matched = teams.displayEmtpyTeams();
            }

            if (matched == false)
                Console.WriteLine("- All teams have staff members.");              
            /*=====================================End of 9. List GROUP(s) with no associated PARENT/CHILD object(s)====================================*/
            
            /*======================================== Additional Functionality ========================================*/
            
            //Display total yearly wage budget for each team
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\nBelow are the total yearly wage bill's for the teams. (Additional Function)");
            Console.ForegroundColor = ConsoleColor.White;

            foreach (Team team in L1.teams)
            {
                team.displayTotalWages();
            }
            

            //Display all players who are strikers
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\nBelow are all of the players in La Liga who are Strikers. (Additional Function)");
            Console.ForegroundColor = ConsoleColor.White;

            L1.listPlayers("ST");


            //List all players in descending salary order
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\nHere are the players, ordered by salary. (Additional Function)");
            Console.ForegroundColor = ConsoleColor.White;

            L1.sortWages();

            
            Console.ReadLine();
        }

    }
}
