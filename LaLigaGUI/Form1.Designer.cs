﻿namespace LaLigaGUI
{
    partial class laLigaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label nameLabel3;
            System.Windows.Forms.Label teamIDLabel;
            System.Windows.Forms.Label treatmentRoomLabel;
            System.Windows.Forms.Label staffIDLabel2;
            System.Windows.Forms.Label specialisationLabel;
            System.Windows.Forms.Label salaryLabel2;
            System.Windows.Forms.Label nameLabel2;
            System.Windows.Forms.Label staffIDLabel1;
            System.Windows.Forms.Label salaryLabel1;
            System.Windows.Forms.Label nameLabel1;
            System.Windows.Forms.Label jobTitleLabel;
            System.Windows.Forms.Label playerStaffIDLabel;
            System.Windows.Forms.Label playerSalaryLabel;
            System.Windows.Forms.Label playerPositionLabel;
            System.Windows.Forms.Label playerNameLabel;
            System.Windows.Forms.Label playerContractLengthLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(laLigaForm));
            this.panel2 = new System.Windows.Forms.Panel();
            this.teamIDMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.teamBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.teamNameMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.playerPanel = new System.Windows.Forms.Panel();
            this.salaryMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.playerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.playerStaffIDMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.playerPositionMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.contractLengthMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.playerNameMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.playerBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.playerBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.coachPanel = new System.Windows.Forms.Panel();
            this.coachStaffIDMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.coachBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.coachSalaryMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.coachNameMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.coachBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.jobTitleTextBox = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.treatmentRoomMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.treatmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.treatmentStaffIDMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.treatmentSpecialisationMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.treatmentSalaryMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.treatmentNameMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.treatmentBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton13 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton14 = new System.Windows.Forms.ToolStripButton();
            this.teamBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem1 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem1 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.teamBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.teamGroupBox = new System.Windows.Forms.GroupBox();
            this.fileLocationDialog = new System.Windows.Forms.OpenFileDialog();
            this.fileLocationButton = new System.Windows.Forms.Button();
            this.wagesButton = new System.Windows.Forms.Button();
            this.searchPositionButton = new System.Windows.Forms.Button();
            this.totalWagesButton = new System.Windows.Forms.Button();
            this.positionMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            nameLabel3 = new System.Windows.Forms.Label();
            teamIDLabel = new System.Windows.Forms.Label();
            treatmentRoomLabel = new System.Windows.Forms.Label();
            staffIDLabel2 = new System.Windows.Forms.Label();
            specialisationLabel = new System.Windows.Forms.Label();
            salaryLabel2 = new System.Windows.Forms.Label();
            nameLabel2 = new System.Windows.Forms.Label();
            staffIDLabel1 = new System.Windows.Forms.Label();
            salaryLabel1 = new System.Windows.Forms.Label();
            nameLabel1 = new System.Windows.Forms.Label();
            jobTitleLabel = new System.Windows.Forms.Label();
            playerStaffIDLabel = new System.Windows.Forms.Label();
            playerSalaryLabel = new System.Windows.Forms.Label();
            playerPositionLabel = new System.Windows.Forms.Label();
            playerNameLabel = new System.Windows.Forms.Label();
            playerContractLengthLabel = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamBindingSource)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.playerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingNavigator)).BeginInit();
            this.playerBindingNavigator.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.coachPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.coachBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coachBindingNavigator)).BeginInit();
            this.coachBindingNavigator.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treatmentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treatmentBindingNavigator)).BeginInit();
            this.treatmentBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamBindingNavigator)).BeginInit();
            this.teamBindingNavigator.SuspendLayout();
            this.teamGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // nameLabel3
            // 
            nameLabel3.AutoSize = true;
            nameLabel3.Location = new System.Drawing.Point(76, 40);
            nameLabel3.Name = "nameLabel3";
            nameLabel3.Size = new System.Drawing.Size(38, 13);
            nameLabel3.TabIndex = 5;
            nameLabel3.Text = "Name:";
            // 
            // teamIDLabel
            // 
            teamIDLabel.AutoSize = true;
            teamIDLabel.Location = new System.Drawing.Point(76, 67);
            teamIDLabel.Name = "teamIDLabel";
            teamIDLabel.Size = new System.Drawing.Size(51, 13);
            teamIDLabel.TabIndex = 7;
            teamIDLabel.Text = "Team ID:";
            // 
            // treatmentRoomLabel
            // 
            treatmentRoomLabel.AutoSize = true;
            treatmentRoomLabel.Location = new System.Drawing.Point(58, 141);
            treatmentRoomLabel.Name = "treatmentRoomLabel";
            treatmentRoomLabel.Size = new System.Drawing.Size(89, 13);
            treatmentRoomLabel.TabIndex = 8;
            treatmentRoomLabel.Text = "Treatment Room:";
            // 
            // staffIDLabel2
            // 
            staffIDLabel2.AutoSize = true;
            staffIDLabel2.Location = new System.Drawing.Point(58, 115);
            staffIDLabel2.Name = "staffIDLabel2";
            staffIDLabel2.Size = new System.Drawing.Size(46, 13);
            staffIDLabel2.TabIndex = 6;
            staffIDLabel2.Text = "Staff ID:";
            // 
            // specialisationLabel
            // 
            specialisationLabel.AutoSize = true;
            specialisationLabel.Location = new System.Drawing.Point(58, 89);
            specialisationLabel.Name = "specialisationLabel";
            specialisationLabel.Size = new System.Drawing.Size(75, 13);
            specialisationLabel.TabIndex = 4;
            specialisationLabel.Text = "Specialisation:";
            // 
            // salaryLabel2
            // 
            salaryLabel2.AutoSize = true;
            salaryLabel2.Location = new System.Drawing.Point(58, 63);
            salaryLabel2.Name = "salaryLabel2";
            salaryLabel2.Size = new System.Drawing.Size(63, 13);
            salaryLabel2.TabIndex = 2;
            salaryLabel2.Text = "Salary (£M):";
            // 
            // nameLabel2
            // 
            nameLabel2.AutoSize = true;
            nameLabel2.Location = new System.Drawing.Point(58, 37);
            nameLabel2.Name = "nameLabel2";
            nameLabel2.Size = new System.Drawing.Size(38, 13);
            nameLabel2.TabIndex = 0;
            nameLabel2.Text = "Name:";
            // 
            // staffIDLabel1
            // 
            staffIDLabel1.AutoSize = true;
            staffIDLabel1.Location = new System.Drawing.Point(47, 115);
            staffIDLabel1.Name = "staffIDLabel1";
            staffIDLabel1.Size = new System.Drawing.Size(46, 13);
            staffIDLabel1.TabIndex = 6;
            staffIDLabel1.Text = "Staff ID:";
            // 
            // salaryLabel1
            // 
            salaryLabel1.AutoSize = true;
            salaryLabel1.Location = new System.Drawing.Point(47, 89);
            salaryLabel1.Name = "salaryLabel1";
            salaryLabel1.Size = new System.Drawing.Size(63, 13);
            salaryLabel1.TabIndex = 4;
            salaryLabel1.Text = "Salary (£M):";
            // 
            // nameLabel1
            // 
            nameLabel1.AutoSize = true;
            nameLabel1.Location = new System.Drawing.Point(47, 34);
            nameLabel1.Name = "nameLabel1";
            nameLabel1.Size = new System.Drawing.Size(38, 13);
            nameLabel1.TabIndex = 2;
            nameLabel1.Text = "Name:";
            // 
            // jobTitleLabel
            // 
            jobTitleLabel.AutoSize = true;
            jobTitleLabel.Location = new System.Drawing.Point(47, 63);
            jobTitleLabel.Name = "jobTitleLabel";
            jobTitleLabel.Size = new System.Drawing.Size(50, 13);
            jobTitleLabel.TabIndex = 0;
            jobTitleLabel.Text = "Job Title:";
            // 
            // playerStaffIDLabel
            // 
            playerStaffIDLabel.AutoSize = true;
            playerStaffIDLabel.Location = new System.Drawing.Point(40, 139);
            playerStaffIDLabel.Name = "playerStaffIDLabel";
            playerStaffIDLabel.Size = new System.Drawing.Size(46, 13);
            playerStaffIDLabel.TabIndex = 8;
            playerStaffIDLabel.Text = "Staff ID:";
            // 
            // playerSalaryLabel
            // 
            playerSalaryLabel.AutoSize = true;
            playerSalaryLabel.Location = new System.Drawing.Point(40, 113);
            playerSalaryLabel.Name = "playerSalaryLabel";
            playerSalaryLabel.Size = new System.Drawing.Size(63, 13);
            playerSalaryLabel.TabIndex = 6;
            playerSalaryLabel.Text = "Salary (£M):";
            // 
            // playerPositionLabel
            // 
            playerPositionLabel.AutoSize = true;
            playerPositionLabel.Location = new System.Drawing.Point(40, 87);
            playerPositionLabel.Name = "playerPositionLabel";
            playerPositionLabel.Size = new System.Drawing.Size(47, 13);
            playerPositionLabel.TabIndex = 4;
            playerPositionLabel.Text = "Position:";
            // 
            // playerNameLabel
            // 
            playerNameLabel.AutoSize = true;
            playerNameLabel.Location = new System.Drawing.Point(40, 39);
            playerNameLabel.Name = "playerNameLabel";
            playerNameLabel.Size = new System.Drawing.Size(38, 13);
            playerNameLabel.TabIndex = 2;
            playerNameLabel.Text = "Name:";
            // 
            // playerContractLengthLabel
            // 
            playerContractLengthLabel.AutoSize = true;
            playerContractLengthLabel.Location = new System.Drawing.Point(40, 61);
            playerContractLengthLabel.Name = "playerContractLengthLabel";
            playerContractLengthLabel.Size = new System.Drawing.Size(86, 13);
            playerContractLengthLabel.TabIndex = 0;
            playerContractLengthLabel.Text = "Contract Length:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.teamIDMaskedTextBox);
            this.panel2.Controls.Add(this.teamNameMaskedTextBox);
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.teamBindingNavigator);
            this.panel2.Controls.Add(nameLabel3);
            this.panel2.Controls.Add(teamIDLabel);
            this.panel2.Location = new System.Drawing.Point(14, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(332, 348);
            this.panel2.TabIndex = 6;
            // 
            // teamIDMaskedTextBox
            // 
            this.teamIDMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.teamBindingSource, "teamID", true));
            this.teamIDMaskedTextBox.Location = new System.Drawing.Point(145, 64);
            this.teamIDMaskedTextBox.Mask = "9999999999";
            this.teamIDMaskedTextBox.Name = "teamIDMaskedTextBox";
            this.teamIDMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.teamIDMaskedTextBox.TabIndex = 11;
            // 
            // teamBindingSource
            // 
            this.teamBindingSource.AllowNew = true;
            this.teamBindingSource.DataSource = typeof(LaLiga.Team);
            // 
            // teamNameMaskedTextBox
            // 
            this.teamNameMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.teamBindingSource, "name", true));
            this.teamNameMaskedTextBox.Location = new System.Drawing.Point(145, 37);
            this.teamNameMaskedTextBox.Mask = "LLLLLLLLLLLLLLLLLLLLLLLLLLL";
            this.teamNameMaskedTextBox.Name = "teamNameMaskedTextBox";
            this.teamNameMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.teamNameMaskedTextBox.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 120);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(327, 216);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.playerPanel);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(319, 190);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Player";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // playerPanel
            // 
            this.playerPanel.Controls.Add(this.salaryMaskedTextBox);
            this.playerPanel.Controls.Add(this.playerStaffIDMaskedTextBox);
            this.playerPanel.Controls.Add(this.playerPositionMaskedTextBox);
            this.playerPanel.Controls.Add(this.contractLengthMaskedTextBox);
            this.playerPanel.Controls.Add(this.playerNameMaskedTextBox);
            this.playerPanel.Controls.Add(this.playerBindingNavigator);
            this.playerPanel.Controls.Add(playerContractLengthLabel);
            this.playerPanel.Controls.Add(playerNameLabel);
            this.playerPanel.Controls.Add(playerPositionLabel);
            this.playerPanel.Controls.Add(playerSalaryLabel);
            this.playerPanel.Controls.Add(playerStaffIDLabel);
            this.playerPanel.Location = new System.Drawing.Point(18, 6);
            this.playerPanel.Name = "playerPanel";
            this.playerPanel.Size = new System.Drawing.Size(283, 169);
            this.playerPanel.TabIndex = 0;
            // 
            // salaryMaskedTextBox
            // 
            this.salaryMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playerBindingSource, "salary", true));
            this.salaryMaskedTextBox.Location = new System.Drawing.Point(132, 110);
            this.salaryMaskedTextBox.Name = "salaryMaskedTextBox";
            this.salaryMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.salaryMaskedTextBox.TabIndex = 14;
            // 
            // playerBindingSource
            // 
            this.playerBindingSource.AllowNew = true;
            this.playerBindingSource.DataSource = typeof(LaLiga.Player);
            // 
            // playerStaffIDMaskedTextBox
            // 
            this.playerStaffIDMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playerBindingSource, "staffID", true));
            this.playerStaffIDMaskedTextBox.Location = new System.Drawing.Point(131, 136);
            this.playerStaffIDMaskedTextBox.Mask = "9999999999";
            this.playerStaffIDMaskedTextBox.Name = "playerStaffIDMaskedTextBox";
            this.playerStaffIDMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.playerStaffIDMaskedTextBox.TabIndex = 13;
            // 
            // playerPositionMaskedTextBox
            // 
            this.playerPositionMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playerBindingSource, "position", true));
            this.playerPositionMaskedTextBox.Location = new System.Drawing.Point(131, 84);
            this.playerPositionMaskedTextBox.Mask = "LLL";
            this.playerPositionMaskedTextBox.Name = "playerPositionMaskedTextBox";
            this.playerPositionMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.playerPositionMaskedTextBox.TabIndex = 13;
            // 
            // contractLengthMaskedTextBox
            // 
            this.contractLengthMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playerBindingSource, "contractLength", true));
            this.contractLengthMaskedTextBox.Location = new System.Drawing.Point(132, 58);
            this.contractLengthMaskedTextBox.Mask = "999";
            this.contractLengthMaskedTextBox.Name = "contractLengthMaskedTextBox";
            this.contractLengthMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.contractLengthMaskedTextBox.TabIndex = 12;
            // 
            // playerNameMaskedTextBox
            // 
            this.playerNameMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playerBindingSource, "name", true));
            this.playerNameMaskedTextBox.Location = new System.Drawing.Point(131, 32);
            this.playerNameMaskedTextBox.Mask = "LLLLLLLLLLLLLLLLLLLLLLLLLLL";
            this.playerNameMaskedTextBox.Name = "playerNameMaskedTextBox";
            this.playerNameMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.playerNameMaskedTextBox.TabIndex = 12;
            // 
            // playerBindingNavigator
            // 
            this.playerBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.playerBindingNavigator.BindingSource = this.playerBindingSource;
            this.playerBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.playerBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.playerBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.playerBindingNavigatorSaveItem});
            this.playerBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.playerBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.playerBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.playerBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.playerBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.playerBindingNavigator.Name = "playerBindingNavigator";
            this.playerBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.playerBindingNavigator.Size = new System.Drawing.Size(283, 25);
            this.playerBindingNavigator.TabIndex = 10;
            this.playerBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // playerBindingNavigatorSaveItem
            // 
            this.playerBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.playerBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("playerBindingNavigatorSaveItem.Image")));
            this.playerBindingNavigatorSaveItem.Name = "playerBindingNavigatorSaveItem";
            this.playerBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.playerBindingNavigatorSaveItem.Text = "Save Data";
            this.playerBindingNavigatorSaveItem.Click += new System.EventHandler(this.playerBindingNavigatorSaveItem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.coachPanel);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(319, 190);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Coach";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // coachPanel
            // 
            this.coachPanel.Controls.Add(this.coachStaffIDMaskedTextBox);
            this.coachPanel.Controls.Add(this.coachSalaryMaskedTextBox);
            this.coachPanel.Controls.Add(this.coachNameMaskedTextBox);
            this.coachPanel.Controls.Add(this.coachBindingNavigator);
            this.coachPanel.Controls.Add(jobTitleLabel);
            this.coachPanel.Controls.Add(this.jobTitleTextBox);
            this.coachPanel.Controls.Add(nameLabel1);
            this.coachPanel.Controls.Add(salaryLabel1);
            this.coachPanel.Controls.Add(staffIDLabel1);
            this.coachPanel.Location = new System.Drawing.Point(17, 6);
            this.coachPanel.Name = "coachPanel";
            this.coachPanel.Size = new System.Drawing.Size(279, 146);
            this.coachPanel.TabIndex = 1;
            // 
            // coachStaffIDMaskedTextBox
            // 
            this.coachStaffIDMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.coachBindingSource, "staffID", true));
            this.coachStaffIDMaskedTextBox.Location = new System.Drawing.Point(116, 112);
            this.coachStaffIDMaskedTextBox.Mask = "9999999999";
            this.coachStaffIDMaskedTextBox.Name = "coachStaffIDMaskedTextBox";
            this.coachStaffIDMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.coachStaffIDMaskedTextBox.TabIndex = 16;
            // 
            // coachBindingSource
            // 
            this.coachBindingSource.AllowNew = true;
            this.coachBindingSource.DataSource = typeof(LaLiga.Coach);
            // 
            // coachSalaryMaskedTextBox
            // 
            this.coachSalaryMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.coachBindingSource, "salary", true));
            this.coachSalaryMaskedTextBox.Location = new System.Drawing.Point(116, 86);
            this.coachSalaryMaskedTextBox.Name = "coachSalaryMaskedTextBox";
            this.coachSalaryMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.coachSalaryMaskedTextBox.TabIndex = 15;
            // 
            // coachNameMaskedTextBox
            // 
            this.coachNameMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.coachBindingSource, "name", true));
            this.coachNameMaskedTextBox.Location = new System.Drawing.Point(116, 34);
            this.coachNameMaskedTextBox.Mask = "LLLLLLLLLLLLLLLLLLLLLLLLLLL";
            this.coachNameMaskedTextBox.Name = "coachNameMaskedTextBox";
            this.coachNameMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.coachNameMaskedTextBox.TabIndex = 13;
            // 
            // coachBindingNavigator
            // 
            this.coachBindingNavigator.AddNewItem = this.toolStripButton1;
            this.coachBindingNavigator.BindingSource = this.coachBindingSource;
            this.coachBindingNavigator.CountItem = this.toolStripLabel1;
            this.coachBindingNavigator.DeleteItem = this.toolStripButton2;
            this.coachBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripSeparator3,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton7});
            this.coachBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.coachBindingNavigator.MoveFirstItem = this.toolStripButton3;
            this.coachBindingNavigator.MoveLastItem = this.toolStripButton6;
            this.coachBindingNavigator.MoveNextItem = this.toolStripButton5;
            this.coachBindingNavigator.MovePreviousItem = this.toolStripButton4;
            this.coachBindingNavigator.Name = "coachBindingNavigator";
            this.coachBindingNavigator.PositionItem = this.toolStripTextBox1;
            this.coachBindingNavigator.Size = new System.Drawing.Size(279, 25);
            this.coachBindingNavigator.TabIndex = 11;
            this.coachBindingNavigator.Text = "bindingNavigator1";
            this.coachBindingNavigator.Click += new System.EventHandler(this.coachBindingNavigator_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Add new";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel1.Text = "of {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.RightToLeftAutoMirrorImage = true;
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Delete";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.RightToLeftAutoMirrorImage = true;
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Move first";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.RightToLeftAutoMirrorImage = true;
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "Move previous";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "Move next";
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.RightToLeftAutoMirrorImage = true;
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "Move last";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton7.Text = "Save Data";
            // 
            // jobTitleTextBox
            // 
            this.jobTitleTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.coachBindingSource, "jobTitle", true));
            this.jobTitleTextBox.Location = new System.Drawing.Point(116, 60);
            this.jobTitleTextBox.Name = "jobTitleTextBox";
            this.jobTitleTextBox.Size = new System.Drawing.Size(100, 20);
            this.jobTitleTextBox.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(319, 190);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Treatment staff";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.treatmentRoomMaskedTextBox);
            this.panel1.Controls.Add(this.treatmentStaffIDMaskedTextBox);
            this.panel1.Controls.Add(this.treatmentSpecialisationMaskedTextBox);
            this.panel1.Controls.Add(this.treatmentSalaryMaskedTextBox);
            this.panel1.Controls.Add(this.treatmentNameMaskedTextBox);
            this.panel1.Controls.Add(this.treatmentBindingNavigator);
            this.panel1.Controls.Add(nameLabel2);
            this.panel1.Controls.Add(salaryLabel2);
            this.panel1.Controls.Add(specialisationLabel);
            this.panel1.Controls.Add(staffIDLabel2);
            this.panel1.Controls.Add(treatmentRoomLabel);
            this.panel1.Location = new System.Drawing.Point(16, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(279, 168);
            this.panel1.TabIndex = 2;
            // 
            // treatmentRoomMaskedTextBox
            // 
            this.treatmentRoomMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.treatmentBindingSource, "treatmentRoom", true));
            this.treatmentRoomMaskedTextBox.Location = new System.Drawing.Point(149, 138);
            this.treatmentRoomMaskedTextBox.Mask = "L999";
            this.treatmentRoomMaskedTextBox.Name = "treatmentRoomMaskedTextBox";
            this.treatmentRoomMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.treatmentRoomMaskedTextBox.TabIndex = 19;
            // 
            // treatmentBindingSource
            // 
            this.treatmentBindingSource.AllowNew = true;
            this.treatmentBindingSource.DataSource = typeof(LaLiga.Treatment);
            // 
            // treatmentStaffIDMaskedTextBox
            // 
            this.treatmentStaffIDMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.treatmentBindingSource, "staffID", true));
            this.treatmentStaffIDMaskedTextBox.Location = new System.Drawing.Point(149, 112);
            this.treatmentStaffIDMaskedTextBox.Mask = "9999999999";
            this.treatmentStaffIDMaskedTextBox.Name = "treatmentStaffIDMaskedTextBox";
            this.treatmentStaffIDMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.treatmentStaffIDMaskedTextBox.TabIndex = 18;
            // 
            // treatmentSpecialisationMaskedTextBox
            // 
            this.treatmentSpecialisationMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.treatmentBindingSource, "specialisation", true));
            this.treatmentSpecialisationMaskedTextBox.Location = new System.Drawing.Point(149, 86);
            this.treatmentSpecialisationMaskedTextBox.Mask = "LLLLLLLLLLLLLLLL";
            this.treatmentSpecialisationMaskedTextBox.Name = "treatmentSpecialisationMaskedTextBox";
            this.treatmentSpecialisationMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.treatmentSpecialisationMaskedTextBox.TabIndex = 17;
            // 
            // treatmentSalaryMaskedTextBox
            // 
            this.treatmentSalaryMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.treatmentBindingSource, "salary", true));
            this.treatmentSalaryMaskedTextBox.Location = new System.Drawing.Point(149, 60);
            this.treatmentSalaryMaskedTextBox.Name = "treatmentSalaryMaskedTextBox";
            this.treatmentSalaryMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.treatmentSalaryMaskedTextBox.TabIndex = 16;
            // 
            // treatmentNameMaskedTextBox
            // 
            this.treatmentNameMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.treatmentBindingSource, "name", true));
            this.treatmentNameMaskedTextBox.Location = new System.Drawing.Point(149, 34);
            this.treatmentNameMaskedTextBox.Mask = "LLLLLLLLLLLLLLLLLLLLLLLLLLL";
            this.treatmentNameMaskedTextBox.Name = "treatmentNameMaskedTextBox";
            this.treatmentNameMaskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.treatmentNameMaskedTextBox.TabIndex = 14;
            // 
            // treatmentBindingNavigator
            // 
            this.treatmentBindingNavigator.AddNewItem = this.toolStripButton8;
            this.treatmentBindingNavigator.BindingSource = this.treatmentBindingSource;
            this.treatmentBindingNavigator.CountItem = this.toolStripLabel2;
            this.treatmentBindingNavigator.DeleteItem = this.toolStripButton9;
            this.treatmentBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton10,
            this.toolStripButton11,
            this.toolStripSeparator4,
            this.toolStripTextBox2,
            this.toolStripLabel2,
            this.toolStripSeparator5,
            this.toolStripButton12,
            this.toolStripButton13,
            this.toolStripSeparator6,
            this.toolStripButton8,
            this.toolStripButton9,
            this.toolStripButton14});
            this.treatmentBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.treatmentBindingNavigator.MoveFirstItem = this.toolStripButton10;
            this.treatmentBindingNavigator.MoveLastItem = this.toolStripButton13;
            this.treatmentBindingNavigator.MoveNextItem = this.toolStripButton12;
            this.treatmentBindingNavigator.MovePreviousItem = this.toolStripButton11;
            this.treatmentBindingNavigator.Name = "treatmentBindingNavigator";
            this.treatmentBindingNavigator.PositionItem = this.toolStripTextBox2;
            this.treatmentBindingNavigator.Size = new System.Drawing.Size(279, 25);
            this.treatmentBindingNavigator.TabIndex = 11;
            this.treatmentBindingNavigator.Text = "bindingNavigator1";
            this.treatmentBindingNavigator.Click += new System.EventHandler(this.treatmentBindingNavigator_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.RightToLeftAutoMirrorImage = true;
            this.toolStripButton8.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton8.Text = "Add new";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel2.Text = "of {0}";
            this.toolStripLabel2.ToolTipText = "Total number of items";
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.RightToLeftAutoMirrorImage = true;
            this.toolStripButton9.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton9.Text = "Delete";
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10.Image")));
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.RightToLeftAutoMirrorImage = true;
            this.toolStripButton10.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton10.Text = "Move first";
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton11.Image")));
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.RightToLeftAutoMirrorImage = true;
            this.toolStripButton11.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton11.Text = "Move previous";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.AccessibleName = "Position";
            this.toolStripTextBox2.AutoSize = false;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox2.Text = "0";
            this.toolStripTextBox2.ToolTipText = "Current position";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton12
            // 
            this.toolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton12.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton12.Image")));
            this.toolStripButton12.Name = "toolStripButton12";
            this.toolStripButton12.RightToLeftAutoMirrorImage = true;
            this.toolStripButton12.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton12.Text = "Move next";
            // 
            // toolStripButton13
            // 
            this.toolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton13.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton13.Image")));
            this.toolStripButton13.Name = "toolStripButton13";
            this.toolStripButton13.RightToLeftAutoMirrorImage = true;
            this.toolStripButton13.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton13.Text = "Move last";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton14
            // 
            this.toolStripButton14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton14.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton14.Image")));
            this.toolStripButton14.Name = "toolStripButton14";
            this.toolStripButton14.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton14.Text = "Save Data";
            // 
            // teamBindingNavigator
            // 
            this.teamBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem1;
            this.teamBindingNavigator.BindingSource = this.teamBindingSource;
            this.teamBindingNavigator.CountItem = this.bindingNavigatorCountItem1;
            this.teamBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem1;
            this.teamBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem1,
            this.bindingNavigatorMovePreviousItem1,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPositionItem1,
            this.bindingNavigatorCountItem1,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorMoveNextItem1,
            this.bindingNavigatorMoveLastItem1,
            this.bindingNavigatorSeparator5,
            this.bindingNavigatorAddNewItem1,
            this.bindingNavigatorDeleteItem1,
            this.teamBindingNavigatorSaveItem});
            this.teamBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.teamBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem1;
            this.teamBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem1;
            this.teamBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem1;
            this.teamBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem1;
            this.teamBindingNavigator.Name = "teamBindingNavigator";
            this.teamBindingNavigator.PositionItem = this.bindingNavigatorPositionItem1;
            this.teamBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.teamBindingNavigator.Size = new System.Drawing.Size(332, 25);
            this.teamBindingNavigator.TabIndex = 9;
            this.teamBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem1
            // 
            this.bindingNavigatorAddNewItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem1.Image")));
            this.bindingNavigatorAddNewItem1.Name = "bindingNavigatorAddNewItem1";
            this.bindingNavigatorAddNewItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem1.Text = "Add new";
            this.bindingNavigatorAddNewItem1.Click += new System.EventHandler(this.bindingNavigatorAddNewItem1_Click);
            // 
            // bindingNavigatorCountItem1
            // 
            this.bindingNavigatorCountItem1.Name = "bindingNavigatorCountItem1";
            this.bindingNavigatorCountItem1.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem1.Text = "of {0}";
            this.bindingNavigatorCountItem1.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem1
            // 
            this.bindingNavigatorDeleteItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem1.Image")));
            this.bindingNavigatorDeleteItem1.Name = "bindingNavigatorDeleteItem1";
            this.bindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem1.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem1
            // 
            this.bindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem1.Image")));
            this.bindingNavigatorMoveFirstItem1.Name = "bindingNavigatorMoveFirstItem1";
            this.bindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem1.Text = "Move first";
            this.bindingNavigatorMoveFirstItem1.Click += new System.EventHandler(this.bindingNavigatorMoveFirstItem1_Click);
            // 
            // bindingNavigatorMovePreviousItem1
            // 
            this.bindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem1.Image")));
            this.bindingNavigatorMovePreviousItem1.Name = "bindingNavigatorMovePreviousItem1";
            this.bindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem1.Text = "Move previous";
            this.bindingNavigatorMovePreviousItem1.Click += new System.EventHandler(this.bindingNavigatorMovePreviousItem1_Click);
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem1
            // 
            this.bindingNavigatorPositionItem1.AccessibleName = "Position";
            this.bindingNavigatorPositionItem1.AutoSize = false;
            this.bindingNavigatorPositionItem1.Name = "bindingNavigatorPositionItem1";
            this.bindingNavigatorPositionItem1.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem1.Text = "0";
            this.bindingNavigatorPositionItem1.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem1
            // 
            this.bindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem1.Image")));
            this.bindingNavigatorMoveNextItem1.Name = "bindingNavigatorMoveNextItem1";
            this.bindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem1.Text = "Move next";
            this.bindingNavigatorMoveNextItem1.Click += new System.EventHandler(this.bindingNavigatorMoveNextItem1_Click);
            // 
            // bindingNavigatorMoveLastItem1
            // 
            this.bindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem1.Image")));
            this.bindingNavigatorMoveLastItem1.Name = "bindingNavigatorMoveLastItem1";
            this.bindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem1.Text = "Move last";
            this.bindingNavigatorMoveLastItem1.Click += new System.EventHandler(this.bindingNavigatorMoveLastItem1_Click);
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // teamBindingNavigatorSaveItem
            // 
            this.teamBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.teamBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("teamBindingNavigatorSaveItem.Image")));
            this.teamBindingNavigatorSaveItem.Name = "teamBindingNavigatorSaveItem";
            this.teamBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.teamBindingNavigatorSaveItem.Text = "Save Data";
            this.teamBindingNavigatorSaveItem.Click += new System.EventHandler(this.teamBindingNavigatorSaveItem_Click);
            // 
            // teamGroupBox
            // 
            this.teamGroupBox.Controls.Add(this.panel2);
            this.teamGroupBox.Location = new System.Drawing.Point(12, 12);
            this.teamGroupBox.Name = "teamGroupBox";
            this.teamGroupBox.Size = new System.Drawing.Size(358, 377);
            this.teamGroupBox.TabIndex = 7;
            this.teamGroupBox.TabStop = false;
            this.teamGroupBox.Text = "Team Details";
            // 
            // fileLocationDialog
            // 
            this.fileLocationDialog.FileName = "openFileDialog1";
            // 
            // fileLocationButton
            // 
            this.fileLocationButton.Location = new System.Drawing.Point(129, 462);
            this.fileLocationButton.Name = "fileLocationButton";
            this.fileLocationButton.Size = new System.Drawing.Size(120, 27);
            this.fileLocationButton.TabIndex = 8;
            this.fileLocationButton.Text = "Change File Location";
            this.fileLocationButton.UseVisualStyleBackColor = true;
            this.fileLocationButton.Click += new System.EventHandler(this.fileLocationButton_Click);
            // 
            // wagesButton
            // 
            this.wagesButton.Location = new System.Drawing.Point(26, 427);
            this.wagesButton.Name = "wagesButton";
            this.wagesButton.Size = new System.Drawing.Size(82, 29);
            this.wagesButton.TabIndex = 9;
            this.wagesButton.Text = "Top Wages";
            this.wagesButton.UseVisualStyleBackColor = true;
            this.wagesButton.Click += new System.EventHandler(this.wagesButton_Click);
            // 
            // searchPositionButton
            // 
            this.searchPositionButton.Location = new System.Drawing.Point(144, 427);
            this.searchPositionButton.Name = "searchPositionButton";
            this.searchPositionButton.Size = new System.Drawing.Size(91, 29);
            this.searchPositionButton.TabIndex = 10;
            this.searchPositionButton.Text = "Search Position";
            this.searchPositionButton.UseVisualStyleBackColor = true;
            this.searchPositionButton.Click += new System.EventHandler(this.searchPositionButton_Click);
            // 
            // totalWagesButton
            // 
            this.totalWagesButton.Location = new System.Drawing.Point(276, 427);
            this.totalWagesButton.Name = "totalWagesButton";
            this.totalWagesButton.Size = new System.Drawing.Size(82, 29);
            this.totalWagesButton.TabIndex = 12;
            this.totalWagesButton.Text = "Total Wages";
            this.totalWagesButton.UseVisualStyleBackColor = true;
            this.totalWagesButton.Click += new System.EventHandler(this.totalWagesButton_Click);
            // 
            // positionMaskedTextBox
            // 
            this.positionMaskedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playerBindingSource, "position", true));
            this.positionMaskedTextBox.Location = new System.Drawing.Point(144, 401);
            this.positionMaskedTextBox.Mask = "LLL";
            this.positionMaskedTextBox.Name = "positionMaskedTextBox";
            this.positionMaskedTextBox.Size = new System.Drawing.Size(91, 20);
            this.positionMaskedTextBox.TabIndex = 14;
            // 
            // laLigaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 501);
            this.Controls.Add(this.positionMaskedTextBox);
            this.Controls.Add(this.totalWagesButton);
            this.Controls.Add(this.searchPositionButton);
            this.Controls.Add(this.wagesButton);
            this.Controls.Add(this.fileLocationButton);
            this.Controls.Add(this.teamGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "laLigaForm";
            this.Text = "La Liga";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamBindingSource)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.playerPanel.ResumeLayout(false);
            this.playerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingNavigator)).EndInit();
            this.playerBindingNavigator.ResumeLayout(false);
            this.playerBindingNavigator.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.coachPanel.ResumeLayout(false);
            this.coachPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.coachBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coachBindingNavigator)).EndInit();
            this.coachBindingNavigator.ResumeLayout(false);
            this.coachBindingNavigator.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treatmentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treatmentBindingNavigator)).EndInit();
            this.treatmentBindingNavigator.ResumeLayout(false);
            this.treatmentBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamBindingNavigator)).EndInit();
            this.teamBindingNavigator.ResumeLayout(false);
            this.teamBindingNavigator.PerformLayout();
            this.teamGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource playerBindingSource;
        private System.Windows.Forms.BindingSource coachBindingSource;
        private System.Windows.Forms.BindingSource treatmentBindingSource;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.BindingNavigator teamBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem1;
        private System.Windows.Forms.BindingSource teamBindingSource;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator3;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator5;
        private System.Windows.Forms.GroupBox teamGroupBox;
        private System.Windows.Forms.ToolStripButton teamBindingNavigatorSaveItem;
        private System.Windows.Forms.OpenFileDialog fileLocationDialog;
        private System.Windows.Forms.Button fileLocationButton;
        private System.Windows.Forms.Button wagesButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel playerPanel;
        private System.Windows.Forms.BindingNavigator playerBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton playerBindingNavigatorSaveItem;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel coachPanel;
        private System.Windows.Forms.BindingNavigator coachBindingNavigator;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.TextBox jobTitleTextBox;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.BindingNavigator treatmentBindingNavigator;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton12;
        private System.Windows.Forms.ToolStripButton toolStripButton13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButton14;
        private System.Windows.Forms.Button searchPositionButton;
        private System.Windows.Forms.Button totalWagesButton;
        private System.Windows.Forms.MaskedTextBox teamNameMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox teamIDMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox playerStaffIDMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox playerPositionMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox contractLengthMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox playerNameMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox salaryMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox coachStaffIDMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox coachSalaryMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox coachNameMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox positionMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox treatmentRoomMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox treatmentStaffIDMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox treatmentSpecialisationMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox treatmentSalaryMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox treatmentNameMaskedTextBox;
    }
}

