﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using LaLiga;

namespace LaLigaGUI
{
    public partial class laLigaForm : Form
    {    
        League L1 = new League();

        public laLigaForm()
        {
            InitializeComponent();
            updateForm();
        }

        private void updateForm()
        {
            L1 = L1.LoadData();


            teamBindingSource.DataSource = L1.teams;
            playerBindingSource.DataSource = L1.teams[Convert.ToInt32(teamBindingNavigator.PositionItem.Text) - 1].players;
            coachBindingSource.DataSource = L1.teams[Convert.ToInt32(teamBindingNavigator.PositionItem.Text) - 1].coaches;
            treatmentBindingSource.DataSource = L1.teams[Convert.ToInt32(teamBindingNavigator.PositionItem.Text) - 1].treatmentStaff;
        }

        private void bindingNavigatorMoveNextItem1_Click(object sender, EventArgs e)
        {
            L1.saveData(L1);
            updateForm();            
        }

        private void bindingNavigatorMovePreviousItem1_Click(object sender, EventArgs e)
        {
            L1.saveData(L1);
            updateForm();
        }

        private void bindingNavigatorMoveLastItem1_Click(object sender, EventArgs e)
        {
            L1.saveData(L1);
            updateForm();
        }

        private void bindingNavigatorMoveFirstItem1_Click(object sender, EventArgs e)
        {
            L1.saveData(L1);
            updateForm();
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
        }

        private void bindingNavigatorAddNewItem1_Click(object sender, EventArgs e)
        {
            L1.saveData(L1);
            updateForm();
        }

        private void fileLocationButton_Click(object sender, EventArgs e)
        {
            fileLocationDialog.Title = "Select file location";

            if (fileLocationDialog.ShowDialog() == DialogResult.OK)
            {
                L1.changeFileLocation(fileLocationDialog.FileName);
                updateForm();
                MessageBox.Show("File location updated.");
            }
            else
                MessageBox.Show("This file cannot be used, please select another.");            
        }

        private void teamBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            L1.saveData(L1);
        }

        private void playerBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            L1.saveData(L1);
        }

        private void coachBindingNavigator_Click(object sender, EventArgs e)
        {
            L1.saveData(L1);
        }

        private void treatmentBindingNavigator_Click(object sender, EventArgs e)
        {
            L1.saveData(L1);
        }

        private void wagesButton_Click(object sender, EventArgs e)
        {
            IList<String> allStaff = new List<String>();

            allStaff = L1.sortWages();

            tabControl1.TabPages.Add("Wages");

            ListBox listBox = new ListBox();
            listBox.Height = 190;
            listBox.Width = 320;
            listBox.HorizontalScrollbar = true;
            listBox.DataSource = allStaff;

            tabControl1.TabPages[tabControl1.TabCount-1].Controls.Add(listBox);

            wagesButton.Enabled = false;

            tabControl1.SelectedTab = tabControl1.TabPages[tabControl1.TabCount - 1];
        }

        private void searchPositionButton_Click(object sender, EventArgs e)
        {
            IList<String> players = new List<String>();

            players = L1.listPlayers(positionMaskedTextBox.Text);

            tabControl1.TabPages.Add("Found Players");

            ListBox listBox = new ListBox();
            listBox.Height = 190;
            listBox.Width = 320;
            listBox.HorizontalScrollbar = true;
            listBox.DataSource = players;

            tabControl1.TabPages[tabControl1.TabCount-1].Controls.Add(listBox);

            searchPositionButton.Enabled = false;
            tabControl1.SelectedTab = tabControl1.TabPages[tabControl1.TabCount - 1];
        }

        private void totalWagesButton_Click(object sender, EventArgs e)
        {
            IList<String> teamWages = new List<String>();

            foreach (Team team in L1.teams)
            {
                teamWages.Add(team.displayTotalWages());
            }

            tabControl1.TabPages.Add("Team Wages");

            ListBox listBox = new ListBox();
            listBox.Height = 190;
            listBox.Width = 320;
            listBox.HorizontalScrollbar = true;
            listBox.DataSource = teamWages;

            tabControl1.TabPages[tabControl1.TabCount - 1].Controls.Add(listBox);

            totalWagesButton.Enabled = false;
            tabControl1.SelectedTab = tabControl1.TabPages[tabControl1.TabCount - 1];
        }
    }
}
