﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LaLiga;

namespace LaLigaTests
{
    [TestClass]
    public class ConstructorsTest
    {
        [TestMethod]
        public void Player()
        {
            Player newPlayer = new Player("Gerard Pique", 4, 5, 3, "CB");
            StringAssert.Equals("Gerard Pique", newPlayer.name);
            Assert.AreEqual(4, newPlayer.salary);
            Assert.AreEqual(5, newPlayer.staffID);
            Assert.AreEqual(3, newPlayer.contractLength);
            StringAssert.Equals("CB", newPlayer.position);
        }

        [TestMethod]
        public void Coach()
        {
            Coach newCoach = new Coach("Carlo Ancelotti", 1, 5, "Head Coach");
            StringAssert.Equals("Carlo Ancelotti", newCoach.name);
            Assert.AreEqual(1, newCoach.salary);
            Assert.AreEqual(5, newCoach.staffID);
            StringAssert.Equals("Head Coach", newCoach.jobTitle);
        }

        [TestMethod]
        public void Treatment()
        {
            Treatment newTreatment = new Treatment("Eva Carneiro", 1, 6, "Shoulder", "S102");
            StringAssert.Equals("Eva Carneiro", newTreatment.name);
            Assert.AreEqual(1, newTreatment.salary);
            Assert.AreEqual(6, newTreatment.staffID);
            StringAssert.Equals("Shoulder", newTreatment.specialisation);
            StringAssert.Equals("S102", newTreatment.treatmentRoom);
        }
    }
}
