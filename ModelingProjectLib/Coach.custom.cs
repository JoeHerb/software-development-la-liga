﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaLiga
{
    [Serializable]
    public partial class Coach : Staff
    {
        public Coach() { }

        public Coach(string initName, float initSalary, int initStaffID, string initJobTitle)
        {
            name = initName;
            salary = initSalary;
            staffID = initStaffID;
            jobTitle = initJobTitle;
        }

        public override string ToString()
        {
            return ("\tName: " + name + "\tSalary (£M): " + salary + "\tStaffID: " + staffID + "\tJob Title: " + jobTitle + "\n");
        }


    }
}
