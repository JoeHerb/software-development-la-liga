﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool
//     Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace LaLiga
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;

	public partial class League
	{
		public virtual string name
		{
			get;
			set;
		}

		public virtual string country
		{
			get;
			set;
		}

		public virtual IEnumerable<Team> Team
		{
			get;
			set;
		}

	}
}

