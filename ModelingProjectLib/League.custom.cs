﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace LaLiga
{
    [Serializable]
	public partial class League
	{
        public IList<Team> teams;
        private string uniPath = @"F:\My Documents\Work\Year 2\Software Development\Term 2\Software Development\Coursework 2\LaLigaConsole\L1-data.dat";

        public League(string initName, string initCountry, string initTeamName, int initTeamID)
        {
            name = initName;
            country = initCountry;

            teams = new List<Team>();
            teams.Add(new Team(initTeamName, initTeamID));
        }

        public League(){}

        public void addTeam(string initTeamName,int initTeamID)
        {
            teams.Add(new Team(initTeamName, initTeamID));
        }

        public void removeTeam(int teamToRemove)
        {
            if ((teams[teamToRemove].coaches.Count == 0) && (teams[teamToRemove].players.Count == 0) && (teams[teamToRemove].treatmentStaff.Count == 0))
            {
                teams.Remove(teams[teamToRemove]);

                foreach (Team team in teams)
                {
                    Console.WriteLine(team.ToString());

                    team.displayPlayers();
                    team.displayCoaches();
                    team.displayTreatmentStaff();
                }
            }
            else
            {
                Console.WriteLine("\n- Cannot remove " + teams[teamToRemove].name + " because it has " + teams[teamToRemove].coaches.Count + " coaches, " + teams[teamToRemove].players.Count + " players, " + teams[teamToRemove].treatmentStaff.Count + " treatment staff.\n");
            }
        }

        public void displayAll()
        {
            Console.WriteLine("League: " + name + "\t\tCountry: " + country + "\n");

            foreach (Team team in teams)
            {
                team.displayAll();
            }
        }

        public void movePlayer(int from, int to, int who)
        {

            Console.WriteLine(teams[from].ToString());
            teams[from].displayPlayers();
            Console.WriteLine(teams[to].ToString());
            teams[to].displayPlayers();

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\nMoving " + teams[from].players[who].name + " from Athletico Madrid to Barcelona...\n\n");
            Console.ForegroundColor = ConsoleColor.White;

            teams[to].players.Add(teams[from].players[who]);
            teams[from].players.Remove(teams[from].players[who]);

            Console.WriteLine(teams[from].ToString());
            teams[from].displayPlayers();
            Console.WriteLine(teams[to].ToString());
            teams[to].displayPlayers();
        }

        public IList<String> listPlayers(string searchPosition)
        {
            IList<String> players = new List<String>();

            foreach (Team team in teams)
            {
                foreach (Player player in team.players)
                {
                    if (player.position == searchPosition)
                    {
                        Console.WriteLine("\n- " + player.name + ": \t" + team.name);
                        players.Add("\n- " + player.name + ": \t" + team.name);
                    }
                }
            }

            return players;
        }

        public IList<String> sortWages()
        {
            IList<Staff> allStaff = new List<Staff>();
            IList<String> staffWages = new List<String>();

            foreach (Team team in teams)
            {
                foreach (Player player in team.players)
                {
                    allStaff.Add(player);
                }

                foreach (Coach coach in team.coaches)
                {
                    allStaff.Add(coach);
                }

                foreach (Treatment treatmentStaffMember in team.treatmentStaff)
                {
                    allStaff.Add(treatmentStaffMember);
                }
            }

            var salaries = from staff in allStaff
                           orderby staff.salary descending
                           select staff;

            allStaff = salaries.ToList();

            foreach (Staff staff in allStaff)
            {
                Console.WriteLine("\n- " + staff.name + ":   \t£" + staff.salary + "M");
                staffWages.Add("\n- " + staff.name + ":   \t£" + staff.salary + "M");
            }

            return staffWages;
        }

        public League LoadData()
        {
            FileStream fs = new FileStream(uniPath, FileMode.Open, FileAccess.Read);
            BinaryFormatter bf = new BinaryFormatter();
            League league = (League)bf.Deserialize(fs);
            fs.Close();

            return league;
        }

        public void saveData(League league)
        {
            FileStream fs = new FileStream(uniPath, FileMode.Create, FileAccess.Write);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, league);
            fs.Close();
        }

        public void changeFileLocation(string newPath)
        {
            uniPath = newPath;
        }

        public override string ToString()
        {
            return ("League: " + name + "\t\tCountry: " + country +"\n");
        }
    }
}
