﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaLiga
{
    [Serializable]
    public partial class Player : Staff
    {
        public Player() { }

        public Player(string initName, float initSalary, int initStaffID, int initContractLength, string initPosition)
        {
            name = initName;
            salary = initSalary;
            staffID = initStaffID;
            contractLength = initContractLength;
            position = initPosition;
        }

        public override string ToString()
        {
            return ("\t" + name + "\tSalary (£M): " + salary + "\t\tStaffID: " + staffID + "\tContract Length (Years): " + contractLength + "\tPosition: " + position + "\n");
        }

    }
}
