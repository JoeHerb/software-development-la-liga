﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaLiga
{
    [Serializable]
    public partial class Team
    {
        public IList<Player> players = new List<Player>();
        public IList<Treatment> treatmentStaff = new List<Treatment>();
        public IList<Coach> coaches = new List<Coach>();

        public Team() { }

        public Team(string initName, int initTeamID)
        {
            name = initName;
            teamID = initTeamID;
        }

        public void addPlayer(string initName, float initSalary, int initStaffID, int initContractLength, string initPosition)
        {
            players.Add(new Player(initName, initSalary, initStaffID, initContractLength, initPosition));
        }

        public void addTreatment(string initName, float initSalary, int initStaffID, string initSpecialisation, string initTreatmentRoom)
        {
            treatmentStaff.Add(new Treatment(initName, initSalary, initStaffID, initSpecialisation, initTreatmentRoom));
        }

        public void addCoach(string initName, float initSalary, int initStaffID, string initJobDescription)
        {
            coaches.Add(new Coach(initName, initSalary, initStaffID, initJobDescription));
        }

        public void displayAll()
        {
            //Prints the data for the team
            Console.WriteLine("- Name: " + name + "\tID: " + teamID + "\n");


            //If statement to check wether the team has players
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\tPlayers\n");
            Console.ForegroundColor = ConsoleColor.White;
            if (players.Count > 0)
            {
                //Cycles through and prints the details for each player in the team
                foreach (Player player in players)
                {
                    Console.WriteLine(player.ToString());
                }
            }
            else Console.WriteLine("\tThere are no players associated with this club.\n");

            //If statement to check if the team has coaches
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\tCoaches\n");
            Console.ForegroundColor = ConsoleColor.White;
            if (coaches.Count > 0)
            {
                //Cycles though and prints the details of each coach in the team
                foreach (Coach coach in coaches)
                {
                    Console.WriteLine(coach.ToString());
                }
            }
            else Console.WriteLine("\tThere are no coaching staff associated with this club.\n");

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\tTreatment Staff\n");
            Console.ForegroundColor = ConsoleColor.White;
            //If statement to check if the team has treatment staff
            if (treatmentStaff.Count > 0)
            {
                //Cycles though and prints the details of each member of treatment staff in the team
                foreach (Treatment treatmentStaffMember in treatmentStaff)
                {
                    Console.WriteLine(treatmentStaffMember.ToString());
                }
            }
            else Console.WriteLine("\tThere are no treatment staff associated with this club\n");
        }

        public void displayPlayers()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\tPlayers\n");
            Console.ForegroundColor = ConsoleColor.White;
            if (players.Count > 0)
            {
                //Cycles through and prints the details for each player in the team
                foreach (Player player in players)
                {
                    Console.WriteLine(player.ToString());
                }
            }
            else Console.WriteLine("\tThere are no players associated with this club.\n");
        }

        public void displayCoaches()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\tCoaches\n");
            Console.ForegroundColor = ConsoleColor.White;
            if (coaches.Count > 0)
            {
                //Cycles though and prints the details of each coach in the team
                foreach (Coach coach in coaches)
                {
                    Console.WriteLine(coach.ToString());
                }
            }
            else Console.WriteLine("\tThere are no coaching staff associated with this club.\n");
        }

        public void displayTreatmentStaff()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\tTreatment Staff\n");
            Console.ForegroundColor = ConsoleColor.White;
            //If statement to check if the team has treatment staff
            if (treatmentStaff.Count > 0)
            {
                //Cycles though and prints the details of each member of treatment staff in the team
                foreach (Treatment treatmentStaffMember in treatmentStaff)
                {
                    Console.WriteLine(treatmentStaffMember.ToString());
                }
            }
            else Console.WriteLine("\tThere are no treatment staff associated with this club\n");
        }

        public bool displayEmtpyTeams()
        {
            if ((coaches.Count == 0) && (players.Count == 0) && (treatmentStaff.Count == 0))
            {
                Console.WriteLine("\n- " + name + " has no staff.");
                return true;
            }
            else return false;
        }

        public void displayStaff()
        {
            if (players.Count > 0)
                displayPlayers();
            if (coaches.Count > 0)
                displayCoaches();
            if (treatmentStaff.Count > 0)
                displayTreatmentStaff();
        }

        public void getTeam(string searchName)
        {
            foreach (Player player in players)
            {
                if (player.name == searchName)
                {
                    Console.WriteLine("\n- " + searchName + " plays for: \t" + name + "\n");
                }
            }
        }

        public string displayTotalWages()
        {
            float totalWages = 0;

            foreach(Player player in players)
                totalWages = totalWages + player.salary;
            foreach (Coach coach in coaches)
                totalWages = totalWages + coach.salary;
            foreach (Treatment treatmentStaffMember in treatmentStaff)
                totalWages = totalWages + treatmentStaffMember.salary;

            Console.WriteLine("\n- " + name + ":    \t\t£" + totalWages + "M");
            return ("\n- " + name + ":    \t\t£" + totalWages + "M");
        }

        public void removePlayer(int playerToRemove)
        {
            players.Remove(players[playerToRemove]);
        }

        public override string ToString()
        {
            return ("- Name: " + name + "\tID: " + teamID + "\n");
        }

    }
}
