﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaLiga
{
    [Serializable]
	public partial class Treatment : Staff
	{
        public Treatment() { }

        public Treatment(string initName, float initSalary, int initStaffID, string initSpecialisation, string initTreatmentRoom)
        {
            name = initName;
            salary = initSalary;
            staffID = initStaffID;
            specialisation = initSpecialisation;
            treatmentRoom = initTreatmentRoom;
        }

        public override string ToString()
        {
            return ("\tName: " + name + "\tSalary (£M): " + salary + "\tStaffID: " + staffID + "\tSpecialisation: " + specialisation + "\tRoom: " + treatmentRoom + "\n");
        }

    }
}
